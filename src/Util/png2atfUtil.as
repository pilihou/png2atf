package Util
{
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;

	public class png2atfUtil
	{
		
		private static var nativeProcess:NativeProcess;
		private static var exitCallback:Function;
		private static var logCallback:Function;
		
		public static function pngMerger(texturePackerDir:String,sourceFile:String,exportFile:String,converPngCallBack:Function,logPngCallBack:Function):void
		{
			exitCallback = converPngCallBack;
			logCallback = logPngCallBack;
			var executable:File = new File(texturePackerDir);
			var exportFileArr:Array = exportFile.split("\\");
			var exportFileName:String = exportFileArr[exportFileArr.length-1];
			var exportPath:String = exportFile.replace("\\"+exportFileName,"");
			var params:Vector.<String> = new Vector.<String>();
			params.push("--data");
			params.push(exportPath + "\\"+exportFileName+".xml");
			params.push("--format");
			params.push("sparrow");
			params.push("--sheet");
			params.push(exportPath + "\\"+exportFileName+".png");
			params.push(sourceFile);
			params.push("--size-constraints");
			params.push("POT");

			var info:NativeProcessStartupInfo = new NativeProcessStartupInfo();
			info.arguments = params;
			info.executable = executable;
			
			if(nativeProcess == null){
				nativeProcess = new NativeProcess();
				nativeProcess.addEventListener(NativeProcessExitEvent.EXIT,onExit);
				nativeProcess.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA,onData);
				nativeProcess.addEventListener(ProgressEvent.STANDARD_ERROR_DATA,onError);
			}
			try
			{
				nativeProcess.start(info);
			} 
			catch(error:Error) 
			{
				logCallback(error.getStackTrace()+"\n");
			}
		}
		
		public static function png2atf(sourceFile:String,platform:String, converCallBack:Function, logAtfCallBack:Function):void
		{
			exitCallback = converCallBack;
			logCallback = logAtfCallBack;
			var executable:File = File.applicationDirectory.resolvePath("png2atf.exe");
			
			var exportFileArr:Array = sourceFile.split("\\");
			var exportFileName:String = exportFileArr[exportFileArr.length-1];
			var exportPath:String = sourceFile.replace("\\"+exportFileName,"");
			var params:Vector.<String> = new Vector.<String>();
			if(platform)
			{
				params.push("-c");
				params.push(platform);
			}
			if(platform == "p"){//ios版启用压缩，android不启用否则导致花屏
				params.push("-r");
			}
			params.push("-n");
			params.push("0,0");
			params.push("-i");
			params.push(sourceFile);
			params.push("-o");
			params.push(sourceFile.replace("png","atf"));
			
			var info:NativeProcessStartupInfo = new NativeProcessStartupInfo();
			info.arguments = params;
			info.executable = executable;
			
			if(nativeProcess == null){
				nativeProcess = new NativeProcess();
				nativeProcess.addEventListener(NativeProcessExitEvent.EXIT,onExit);
				nativeProcess.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA,onData);
				nativeProcess.addEventListener(ProgressEvent.STANDARD_ERROR_DATA,onError);
			}
			try
			{
				nativeProcess.start(info);
			} 
			catch(error:Error) 
			{
				logCallback(error.getStackTrace()+"\n");
			}
		}
		
		private static function onExit(e:NativeProcessExitEvent):void{
			exitCallback();
		}
		
		private static function onData(e:ProgressEvent):void{
			logCallback(nativeProcess.standardOutput.readUTFBytes(nativeProcess.standardOutput.bytesAvailable));
		}
		
		private static function onError(e:ProgressEvent):void{
			logCallback(nativeProcess.standardError.readUTFBytes(nativeProcess.standardError.bytesAvailable));
		} 
	}
}