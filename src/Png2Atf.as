package
{
	import com.pilihou.EventType;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.utils.setTimeout;
	
	import Util.png2atfUtil;
	
	[SWF(width=520,height=430)]
	public class Png2Atf extends Sprite
	{
		private var ui:UIPanel;
		
		private var texturePackerDir:String;//源
		private var sourceDir:String;//源
		private var exportDir:String;//目标
		private var platform:String;//平台
		private var compress:Boolean;//是否压缩
		private var mips:Boolean;//是否启用
		private var quality:int;//质量
		
		private var exportFiles:Vector.<File>;
		
		public function Png2Atf()
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			ui = new UIPanel();
			ui.addEventListener(EventType.EVENT_EXPORT_PNG,onExportPNG);
			ui.addEventListener(EventType.EVENT_EXPORT_ATF,onExportATF);
			addChild(ui);
		}
		
		/**
		 * 点击了导出按钮 
		 */		
		private function onExportPNG(e:Event):void{
			ui.exportBtnEnabled = false;
			
			texturePackerDir = ui.texturePackerDir;
			sourceDir = ui.sourceDir;
			exportDir = ui.exportDir;
			platform = ui.platform;
			exportFiles = new Vector.<File>();
			ergodicDirectory2(new File(sourceDir));
			
			ui.clearLogs();
			ui.log("开始导出PNG...\n");
			ui.log("总共选择了"+exportFiles.length+"个文件.\n");
			
			if(exportFiles.length == 0){
				ui.log("导出完毕.\n");
				ui.exportBtnEnabled = true;
			}else{
				setTimeout(function():void{
					startExportPng(exportFiles.pop());
				},600);
			}
		}
		
		private function onExportATF(e:Event):void{
			ui.exportBtnEnabled = false;
			exportDir = ui.exportDir;
			platform = ui.platform;
			exportFiles = new Vector.<File>();
			ergodicDirectory(new File(exportDir));
			ui.clearLogs();
			ui.log("开始导出ATF...\n");
			ui.log("总共选择了"+exportFiles.length+"个文件.\n");
			if(exportFiles.length == 0){
				ui.log("导出完毕.\n");
				ui.exportBtnEnabled = true;
			}else{
				setTimeout(function():void{
					trace(exportFiles.length);
					startExportAtf(exportFiles.pop());
				},600);
			}
		}
		
		/**
		 * 遍历文件夹
		 * */
		private function ergodicDirectory(file:File):void{
			var array:Array = file.getDirectoryListing();
			var subFile:File;
			var length:int = array.length;
			for (var i:int = 0; i < length; i++) {
				subFile = array[i];
				if(subFile.isDirectory)
				{
					ergodicDirectory(subFile);
				}
				else
				{
					if(exportFiles.indexOf(file)==-1&&subFile.url.indexOf(".png")>0) exportFiles.push(subFile);
				}
			}
		}
		
		/**
		 * 遍历文件夹
		 * */
		private function ergodicDirectory2(file:File,parentFile:File=null):void{
			var array:Array = file.getDirectoryListing();
			var subFile:File;
			var length:int = array.length;
			for (var i:int = 0; i < length; i++) {
				subFile = array[i];
				if(subFile.isDirectory)
				{
					ergodicDirectory2(subFile,file);
				}
				else
				{
					if(parentFile!=null) createDir(parentFile);
					if(exportFiles.indexOf(file)==-1) exportFiles.push(file);
				}
			}
		}
		
		/**创建文件夹*/		
		private function createDir(file:File):void{
			var path:String = file.nativePath.replace(sourceDir,exportDir);
			var f:File = new File(path);
			if(!f.exists){
				f.createDirectory();
			}
		}
		
		/**复制文件*/		
		private function copyFile(file:File):void{
			var path:String = file.nativePath.replace(sourceDir,exportDir);
			var f:File = new File(path);
			if(!f.exists){
				file.copyTo(f,true);
			}
		}
		
		private var sourceFile:String;
		private var exportFile:String;
		/**
		 * 开始输出
		 * */
		private function startExportPng(file:File):void{
			ui.log("\n"+file.name + "开始导出...剩余:"+exportFiles.length+"个文件...\n");
			sourceFile = file.nativePath;
			exportFile = sourceFile.replace(sourceDir,exportDir);
			trace(exportFile);
			png2atfUtil.pngMerger(texturePackerDir,sourceFile,exportFile,converAtfCallBack,logCallBack);
			function converAtfCallBack():void{		
				if(exportFiles.length > 0){
					var file:File = exportFiles.pop();
					ui.log(file.url+"\n");
					startExportPng(file);
				}else{
					ui.log("导出完毕.\n");
					ui.exportBtnEnabled = true;
				}
			}
			
			function logCallBack(text:String):void{
				ui.log(text);
			}
		}
		/**
		 * 开始输出ATF
		 * */
		private function startExportAtf(file:File):void{
			ui.log("\n"+file.name + "开始导出...剩余:"+exportFiles.length+"个文件...\n");
			sourceFile = file.nativePath;
			exportFile = sourceFile.replace(sourceDir,exportDir);
			trace(exportFile);
			png2atfUtil.png2atf(sourceFile,platform,converAtfCallBack2,logCallBack2);
			function converAtfCallBack2():void{		
				if(exportFiles.length > 0){
					var file:File = exportFiles.pop();
					ui.log(file.url+"\n");
					startExportAtf(file);
				}else{
					ui.log("导出完毕.\n");
					ui.exportBtnEnabled = true;
				}
			}
			
			function logCallBack2(text:String):void{
				ui.log(text);
			}
		}
	}
}