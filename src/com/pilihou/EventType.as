package com.pilihou
{
	public class EventType
	{
		public static const EVENT_EXPORT_PNG:String = "eventExportPng";
		public static const EVENT_EXPORT_ATF:String = "eventExportAtf";
	}
}